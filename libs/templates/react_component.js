import React from 'react';
import PropTypes from 'prop-types';


/** the [[name]] component */
class [[name]] extends React.Component {

    render() {

        return (

            <div className={"[[name]]Component"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the [[name]] component propTypes for typechecking */
[[name]].propTypes = {

}

export default [[name]]
