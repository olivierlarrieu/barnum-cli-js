import React from 'react';
import Loadable from 'react-loadable';
import store, { createReducer } from '../store'
import Container from './Container'
import { PageContentContainer } from '../containers'


const Loading = (props) => <PageContentContainer><Container><h1>LOADING ...</h1></Container></PageContentContainer>

const LoadableComponent = (path) => Loadable({
    loader: path,
    loading: Loading,
    render(Cmp, props) {
        let Loaded = Cmp.default.component
        let reducer = Cmp.default.reducer
        store.replaceReducer(createReducer(store, reducer))
        return (
            <PageContentContainer>
                <div className={"animated fadeIn"}>
                    <Loaded {...props} />
                </div>
            </PageContentContainer>
        )
    }
});

export default LoadableComponent
