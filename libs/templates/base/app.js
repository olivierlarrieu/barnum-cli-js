import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Link } from 'react-router-dom'
import Resources, { ResourcesProvider } from './resources'
import { MenuContainer } from './containers'
import store from './store'


class App extends React.Component {

    render() {

        return (

            <Provider store={store}>

                <Router>

                    <div className="BodyComponent">

                        <MenuContainer>
                            <div className="MenuHeader"></div>
                                {
                                    Resources.map((resource, index) => {
                                        return <Link key={index} to={resource.path}><div >{resource.name}</div></Link>
                                    })
                                }


                        </MenuContainer>

                        <ResourcesProvider />

                    </div>

                </Router>

            </Provider>
        )
    }

}


export default App

