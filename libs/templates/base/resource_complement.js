]


const ResourcesProvider = () => {
    return (

        <Switch>
        {
            Resources.map((resource, index) => {
                return <Route key={index} path={resource.path} component={resource.component}/>
            })
        }
        </Switch>

    )
}

export default Resources
export { ResourcesProvider }
