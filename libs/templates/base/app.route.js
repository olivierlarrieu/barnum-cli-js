import React from 'react';
import { RootReducer } from './store'
import Container from './components/Container'
import { [[name]]Container } from './containers'

class App extends React.Component {

    render() {

        return (
                
            <Container>

                <[[name]]Container>
                
                <h1>{ this.props.match.path }</h1>

                </[[name]]Container>

            </Container>

        )
    }

}


const Lazy = {
    component: App,
    reducer: RootReducer
}
export default Lazy
