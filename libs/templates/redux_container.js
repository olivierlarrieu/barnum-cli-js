import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import [[name]]Component from '../components/[[name]]';
//actions

/** the [[name]] component */
class [[name]] extends React.Component {

    render() {

        return (

            <[[name]]Component>
            { this.props.children }
            </[[name]]Component>
            
        )
    
    }

}

/** the [[name]] component propTypes for typechecking */
[[name]].propTypes = {

}

//-----------------------------------------------------------------------------
/** the [[name]] component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
    }
}


const [[name]]Container = connect (
    mapStateToProps,
    mapDispatchToProps
)([[name]])
//-----------------------------------------------------------------------------

export { [[name]], [[name]]Container }
