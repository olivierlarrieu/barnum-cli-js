import {  
  combineReducers,
  createStore,
} from 'redux';

//imports

const RootReducer = {
  //reducers
}

const createReducer = (store={}, loadedReducer={}) => {

  store.LOADED_REDUCERS = {...store.LOADED_REDUCERS, ...loadedReducer}
  let newObject = {...RootReducer, ...store.LOADED_REDUCERS}
  let newReducer = combineReducers(
          newObject
  )
  return newReducer
}

const store = createStore(createReducer(), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
store.LOADED_REDUCERS = {}


export { createReducer, RootReducer }
export default store
