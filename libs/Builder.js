const BarnumProject = require('./project')
const BarnumDescription = require('./description')
const BarnumResource = require('./resource')
const BarnumComponent = require('./component')
const BarnumContainer = require('./container')
const BarnumRoute = require('./route')


function buildProject(file) {

    const description = new BarnumDescription(file)
    return new BarnumProject(description).create()

}

function buildContainer(name, props, projectName) {

    return new BarnumContainer(name, props, projectName).create()

}

function buildComponent(name, projectName) {
    
    return new BarnumComponent(name, projectName).create()

}

function buildResource(name, projectName) {

    return new BarnumResource(name, projectName).create()

}

function buildRoute(name, projectName) {

    return new BarnumRoute(name, projectName).create()

}

module.exports = {
    buildProject,
    buildContainer,
    buildComponent,
    buildResource,
    buildRoute
}
