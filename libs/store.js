const fs = require('fs')
const TemplateFromFile = require('./utils/mustache')


class BarnumStore {

    constructor(name, projectName, description) {

        const CWD = process.cwd()
        const SRC_DIRNAME = process.env.BARNUM_SOURCE_DIRNAME
        const SRC = CWD + `/${SRC_DIRNAME}/`

        this.name = name
        this.projectName = projectName
        this.description = description
        this.storeDir = this.projectName
        this.template = this._getTemplate()

    }

    _getTemplate() {

        return new TemplateFromFile(
            __dirname + '/templates/redux_store.js'
        ).render()

    }

    _writeFile(content) {

        fs.writeFileSync(
            this.storeDir + '/store.js',
            content
        )

    }

    _buildContent() {

        let final_content = []
        let lines = this.template.split(/\r\n|\n/)

        lines.forEach((line) => {

            final_content.push(line)
            if (line.includes("//imports")) {

                this.description.containers.forEach((container) => {

                    let new_line = `import { ${container.name}Reducer } from '${this.name}/reducers/${container.name}'`
                    final_content.push(new_line)

                })

            }

            if (line.includes("//reducers")) {

                this.description.containers.forEach((container) => {

                    let new_line = `  ${this.name}_${container.name}: ${container.name}Reducer,`
                    final_content.push(new_line)

                })

            }

        })

        return final_content.join('\n')

    }

    create() {

        let content = this._buildContent()
        this._writeFile(content)

    }

}
module.exports = BarnumStore
