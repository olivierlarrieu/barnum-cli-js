const fs = require('fs')
const TemplateFromFile = require('./utils/mustache')
const BarnumProject = require('./project')
const BarnumContainer = require('./container')
const BarnumComponent = require('./component')

class Data {

    constructor() {

        this.defaultDescription = {
            "isResource": true,
            "isRoute": false,
            "components": [
                {
                    "name": "Container"
                },
            ],
            "resources": [],
            "routes": [],
            "containers": [
                {
                    "name": "List",
                    "props": [
                        {
                            "name": "name",
                            "type": "string",
                            "default": "List"
                        },
                    ]
                },
                {
                    "name": "Detail",
                    "props": [
                        {
                            "name": "name",
                            "type": "string",
                            "default": "Detail"
                        },
                    ]
                },
                {
                    "name": "Edit",
                    "props": [
                        {
                            "name": "name",
                            "type": "string",
                            "default": "Edit"
                        },
                    ]
                },
                {
                    "name": "Filters",
                    "props": [
                        {
                            "name": "name",
                            "type": "string",
                            "default": "Filters"
                        },
                    ]
                }
            ]
        }

    }

}


class BarnumResource extends Data {

    constructor(name, project_name) {

        super()

        const CWD = process.cwd()
        const SRC = CWD + '/src/'
        const RESOURCE_DIRNAME = "Resources"

        this.name = name
        this.project_name = project_name
        this.projectDir = SRC + project_name
        this.resourcesDir = SRC + RESOURCE_DIRNAME

        const description = this._prepareDescription()
        this.project = new BarnumProject(description, `${this.resourcesDir}/${this.name}`)

    }

    _prepareDescription() {

        let description =  this.defaultDescription
        description.name = this.name
        let containers = []
        let components = []
        description.components.forEach((obj) => {

            components.push(new BarnumComponent(obj.name, `Resources/${this.name}`))

        })
        description.components = components

        description.containers.forEach((obj) => {

            containers.push(new BarnumContainer(obj.name, obj.props, `Resources/${this.name}`))

        })
        description.containers = containers
        return description

    }

    addResourceToProject() {

        let resourceTemplate = new TemplateFromFile(
            `${this.projectDir}/resources.js`
        )
        let content = resourceTemplate.render().split(/\r\n|\n/)
        let final_content = []

        content.forEach((line) => {

            final_content.push(line)
            if (line.includes('const Resources = [')) {

                let new_line_1 = `    name: '${this.name}',`
                let new_line_2 = `    icon: Icon,`
                let new_line_3 = `    path: '/${this.name.toLowerCase()}',`
                let new_line_4 = `    component: LoadableComponent(() => { return import('${this.name}/app.resource') })`
                final_content.push('  {')
                final_content.push(new_line_1)
                final_content.push(new_line_2)
                final_content.push(new_line_3)
                final_content.push(new_line_4)
                final_content.push('  },')

            }

        })

        fs.writeFileSync(
            resourceTemplate.path,
            final_content.join('\n')
        )

    }

    create() {

        this.project.create()
        this.addResourceToProject()

    }

}

module.exports = BarnumResource
