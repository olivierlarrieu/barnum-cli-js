const fs = require('fs')
const BarnumStore = require('./store')


class Data {

    constructor() {

        this.dirs = [
            'actions',
            'components',
            'containers',
            'reducers',
        ]

        this.files = [
            'index.js',
            'registerServiceWorker.js',
            'store.js',
            'main.css',
            'resources.js',
            'Loadable.js',
            'libs.js',
            'app.js',
        ]

        this.resourceFiles = [
            'index.js',
            'store.js',
            'main.css',
            'resources.js',
            'Loadable.js',
            'libs.js',
            'app.resource.js',
        ]

        this.routeFiles = [
            'index.js',
            'store.js',
            'main.css',
            'libs.js',
            'app.route.js',
        ]

    }

}


const CWD = process.cwd()
const SRC_DIRNAME = process.env.BARNUM_SOURCE_DIRNAME
const SRC = CWD + '/src/'
const RESOURCE_DIRNAME = "Resources"
const ROUTE_DIRNAME = "Routes"


class BarnumProject extends Data {

    constructor(description, projectDir) {

        super()

        this.isResource = description.isResource
        this.isRoute = description.isRoute
        this.description = description
        this.name = this.description.name
        this.projectDir = projectDir || SRC + this.name
        this.store = new BarnumStore(this.name, this.projectDir, this.description)
        this.resourcesDir = SRC + RESOURCE_DIRNAME
        this.routesDir = SRC + ROUTE_DIRNAME
    }

    createProjectDir() {

        let files = this.files
        if (this.isResource) {

            files = this.resourceFiles

        }
        if (this.isRoute) {

            files = this.routeFiles

        }
        try {
            fs.mkdirSync(this.projectDir)
        } catch(error) {console.log(error)}

        this.dirs.forEach((subdir, idx) => {

            fs.mkdirSync(this.projectDir + '/' + subdir)
            fs.writeFileSync(this.projectDir + '/' + subdir + '/index.js')

        })

        files.forEach((file, idx) => {

            fs.copyFileSync(__dirname + '/templates/base/' + file, this.projectDir + '/' + file)

        })

        if (!this.isResource && !this.isRoute) {

            fs.renameSync(this.projectDir + '/Loadable.js', this.projectDir + '/components/Loadable.js')
            fs.writeFileSync(SRC + 'index.js', `import '${this.name}'\n`)

        }

    }

    createComponent(component) {

        component.create()

    }

    createContainer(container) {

        container.create()

    }

    createStore() {

        this.store.create()

    }

    createResource(resource) {

        try {

            fs.mkdirSync(this.resourcesDir)

        }
        catch (error) {}

        resource.create()

    }

    createRoute(route) {

        try {

            fs.mkdirSync(this.routesDir)

        }
        catch (error) {}

        route.create()

    }

    create() {

        this.createProjectDir()
        this.description.components.forEach((component, idx) => {

            this.createComponent(component)

        })

        this.description.containers.forEach((container, idx) => {

            this.createContainer(container)

        })

        this.description.resources.forEach((resource, idx) => {

            this.createResource(resource)

        })

        this.description.routes.forEach((route, idx) => {

            this.createRoute(route)

        })

        this.createStore()

        return true

    }

}

module.exports = BarnumProject
