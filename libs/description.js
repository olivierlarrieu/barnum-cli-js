const BarnumComponent = require('./component')
const BarnumContainer = require('./container')
const BarnumResource = require('./resource')
const BarnumRoute = require('./route')


class BarnumDescription {

    constructor(file, obj) {

        if (obj === undefined) {

            this._description = require(file)

        }
        else { this._description = obj }

        this.name = this._description.name
        this.is_resource = this._description.isResource || false
        this.components = []
        this.containers = []
        this.resources = []
        this.routes = []
        this._build()

    }

    _build() {

        let _components = this._description.components || []
        let _containers = this._description.containers || []
        let _resources = this._description.resources || []
        let _routes = this._description.routes || []

        _components.forEach((cmp, idx) => {

            this.components.push(new BarnumComponent(cmp.name, this.name))

        });

        _containers.forEach((cmp, idx) => {

            this.containers.push(new BarnumContainer(cmp.name, cmp.props, this.name))

        });

        _resources.forEach((cmp, idx) => {

            this.resources.push(new BarnumResource(cmp.name, this.name))

        });

        _routes.forEach((cmp, idx) => {

            this.routes.push(new BarnumRoute(cmp.name, this.name))

        });

    }

}


module.exports = BarnumDescription
