const fs = require('fs')
const TemplateFromFile = require('./utils/mustache')
const capitalize = require('./utils/capitalize')


class BarnumActionReducer {

    constructor(name, props, projectName) {

        const CWD = process.cwd()
        const SRC_DIRNAME = process.env.BARNUM_SOURCE_DIRNAME
        const SRC = CWD + `/${SRC_DIRNAME}/`

        this.props = props
        this.name = name
        this.projectName = projectName
        this.actionsDir = SRC + this.projectName + '/actions'
        this.reducersDir = SRC + this.projectName + '/reducers'
        this.reducerTemplate = this._getReducerTemplate()

    }

    _getReducerTemplate() {

        return new TemplateFromFile(
            __dirname + '/templates/redux_reducer.js'
        ).render(
            { name: this.name, projectName: this.projectName }
        )

    }

    _writeActionFile(content) {

        fs.mkdirSync(this.actionsDir + '/' + this.name)
        fs.writeFileSync(
            this.actionsDir + '/' + this.name + '/index.js',
            content
        )

    }

    _writeReducerFile(content) {

        fs.writeFileSync(
            this.reducersDir + '/' + this.name + '.js',
            content
        )

    }

    _buildActionFileContent() {

        let final_content = []
        let imports = []
        let actionExports = []

        this.props.forEach((prop) => {

            let actionType = this._formatActionType(prop)
            imports.push(actionType)

        })


        let new_line = `import { ${imports.join(", ")} } from "../../reducers/${this.name}"\n`
        final_content.push(new_line)


        this.props.forEach((prop) => {

            let actionType = this._formatActionType(prop)
            let functionName = `set${capitalize(prop.name)}`
            actionExports.push(functionName)
            let new_line = `const ${functionName} = (value) => {\n\n    return {type: ${actionType}, payload: {${prop.name}: value }}`
            final_content.push(new_line)
            final_content.push("")
            final_content.push("}\n")

        })

        let line_exports = `export { ${actionExports.join(", ")} }\n`
        final_content.push(line_exports)

        return final_content.join('\n')

    }

    _formatActionType(prop) {

        return `SET_${this.projectName.toUpperCase().replace('/', '_')}_${this.name.toUpperCase()}_${prop.name.toUpperCase()}`

    }

    _buildReducerFileContent() {

        let final_content = []
        let lines = this.reducerTemplate.split(/\r\n|\n/)
        let constants = []
        lines.forEach((line) => {

            final_content.push(line)

            if (line.includes("//constants")) {

                this.props.forEach((prop) => {

                    let constant = this._formatActionType(prop)
                    let new_line = `const ${constant} = "${constant}"`
                    final_content.push(new_line)
                    constants.push(constant)

                })

            }

            if (line.includes("//props")) {

                this.props.forEach((prop) => {

                    let new_line = `    ${prop.name}: "${prop.default}",`
                    final_content.push(new_line)

                })

            }

            if (line.includes("//switch")) {

                this.props.forEach((prop) => {

                    let constant = this._formatActionType(prop)
                    let line_one = `        case ${constant}:`
                    let line_two = `            newState = setState(state, { ...action.payload })`
                    let line_three = `            return newState`
                    final_content.push("")
                    final_content.push(line_one)
                    final_content.push(line_two)
                    final_content.push(line_three)

                })

            }

        })

        let line_exports = `export { ${this.name}State, ${this.name}Reducer, ${constants.join(", ")} }`
        final_content.push(line_exports)
        final_content.push("")

        return final_content.join('\n')

    }

    create() {

        // create the reducer file for this container
        let reducerContent = this._buildReducerFileContent()
        this._writeReducerFile(reducerContent)

        // create the action file according to the reducer
        let actionContent = this._buildActionFileContent()
        this._writeActionFile(actionContent)

    }

}


module.exports = BarnumActionReducer
