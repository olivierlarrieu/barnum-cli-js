const chalk = require('chalk')
const figlet = require('figlet')

const chalkFiglet = (text) => {

    return chalk.green(
        figlet.textSync(text, {
            horizontalLayout: 'default',
            verticalLayout: 'default'
        })
    )

}

const displayLogo = (text) => {

    console.log(chalkFiglet(text));

}

module.exports = displayLogo
