const fs = require('fs')
const mustache = require('mustache')


mustache.tags = ["[[", "]]"]


class TemplateFromFile {

    constructor(path) {

        this.source = fs.readFileSync(path, "utf8")
        this.path = path

    }

    render(data) {

        let content = mustache.to_html(this.source, data)
        return content

    }

    write(content) {

        fs.writeFileSync(this.path, content)

    }

}


module.exports = TemplateFromFile
