const chalk = require('chalk')
const displayLogo = require('./cli.logo')


const warning =  (text) => {

    console.log(
        chalk.red(text)
    )

}

const success =  (text) => {

    console.log(
        chalk.green(text)
    )

}

module.exports = {
    "displayLogo": displayLogo,
    "warning": warning,
    "success": success
}
