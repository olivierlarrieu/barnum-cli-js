var fs = require('fs')
var program = require('commander');
var messages = require('./messages')


class argsParser {

    constructor() {

        this.program = program
        this.program
            .version('0.1.0')
            .option('-pr, --project [file]', 'Create project')
            .option('-re, --resource [name] [project]', 'Create resource')
            .option('-rt, --route [name] [project]', 'Create route')
            .option('-co, --component [name]', 'Create component')
            .option('-cr, --container [name]', 'Create container')
            .parse(process.argv);

        if (!process.argv.slice(2).length) {
            this.program.outputHelp();
            process.exit()
        }

        this.projectName = this.program.args[0] || false

    }

    _controlProjectExists() {

        let exists = fs.existsSync(process.env.BARNUM_SOURCE_DIRNAME + '/' + this.projectName)
        if (!exists) {

            messages.warning(`project with name < ${this.projectName} > does not exists.`)
            process.exit()

        }

    }

    controlProject() {

        if (this.program.project && this.program.project != true) { return true }

        return false

    }

    controlComponent() {

        if (this.program.component && this.program.component != true) {

            this._controlProjectExists()

            let exists = fs.existsSync(process.env.BARNUM_SOURCE_DIRNAME + '/' + this.projectName + '/components/' + this.program.component + '.js')
            if (exists) {

                messages.warning(`component with name < ${this.program.component} > already exists.`)
                process.exit()

            }

            return true
        }

        return false

    }

    controlContainer() {

        if (this.program.container && this.program.container != true) {

            this._controlProjectExists()

            let exists = fs.existsSync(process.env.BARNUM_SOURCE_DIRNAME + '/' + this.projectName + '/containers/' + this.program.container + '.js')
            if (exists) {

                messages.warning(`container with name < ${this.program.container} > already exists.`)
                process.exit()

            }

            return true
        }

        return false

    }

    controlResource() {

        if (this.program.resource && this.program.resource != true) {

            this._controlProjectExists()

            let exists = fs.existsSync(process.env.BARNUM_SOURCE_DIRNAME + '/Resources/' + this.program.resource)
            if (exists) {

                messages.warning(`resource with name < ${this.program.resource} > already exists.`)
                process.exit()

            }

            return true
        }

        return false

    }

    controlRoute() {

        if (this.program.route && this.program.route != true) {

            this._controlProjectExists()

            let exists = fs.existsSync(process.env.BARNUM_SOURCE_DIRNAME + '/Routes/' + this.program.route)
            if (exists) {

                messages.warning(`route with name < ${this.program.route} > already exists.`)
                process.exit()

            }

            return true
        }

        return false

    }

}


module.exports = argsParser
