const fs = require('fs')
const TemplateFromFile = require('./utils/mustache')
const capitalize = require('./utils/capitalize')
const BarnumActionReducer = require('./action')
const BarnumComponent = require('./component')


class BarnumContainer {

    constructor(name, props, projectName) {

        const CWD = process.cwd()
        const SRC_DIRNAME = process.env.BARNUM_SOURCE_DIRNAME
        const SRC = CWD + `/${SRC_DIRNAME}/`
        this.props = props
        this.name = name
        this.projectName = projectName
        this.containersDir = SRC + this.projectName + '/containers'
        this.template = this._getTemplate()

    }

    _getTemplate() {

        return new TemplateFromFile(
            __dirname + '/templates/redux_container.js'
        ).render(
            { name: this.name }
        )

    }

    _writeFile(content) {

        fs.writeFileSync(
            this.containersDir + '/' + this.name + '.js',
            content
        )

    }

    _buildComponent() {

        let component = new BarnumComponent(this.name, this.projectName)
        component.create()

    }

    _buildActionsReducers() {

        // build associated action and reducer
        let actionReducer = new BarnumActionReducer(this.name, this.props, this.projectName)
        actionReducer.create()

    }

    _buildFileContent() {

        let final_content = []
        let lines = this.template.split(/\r\n|\n/)

        lines.forEach((line) => {

            final_content.push(line)

            if (line.includes("//actions")) {

                // les fichiers actions seront crées utlérieurement
                let actions = []
                this.props.forEach((prop) => {

                    let string = `set${capitalize(prop.name)}`
                    actions.push(string)

                })

                let new_line = `import { ${actions.join(', ')} } from "../actions/${this.name}"`
                final_content.push(new_line)

            }

            if (line.includes("//mapStateToProps")) {

                this.props.forEach((prop) => {

                    let new_line = `        ${prop.name}: state.${capitalize(this.projectName.replace('Resources/', '').replace('Routes/', ''))}_${this.name}.${prop.name},`
                    final_content.push(new_line)

                })

            }

            if (line.includes("//mapDispatchToProps")) {

                this.props.forEach((prop) => {


                    let new_line = `        set${capitalize(prop.name)}: (value) => { dispatch(set${capitalize(prop.name)}(value)) },`
                    final_content.push(new_line)

                })

            }

        })

        return final_content.join('\n')

    }

    _addImportToIndex() {

        let components = fs.readdirSync(this.containersDir)
        let final_content = []
        let exports_name = []

        components.forEach((cmp) => {

            if (cmp.includes('.js') && cmp != 'index.js') {

                let name = cmp.split('.js')[0]
                let import_name = name + 'Container'
                let new_line = `import { ${import_name} } from './${name}'`
                final_content.push(new_line)
                exports_name.push(import_name)

            }

        })

        let exports_line = `\n\nexport {${exports_name.join(', ')}}\n`
        final_content.push(exports_line)
        let content = final_content.join('\n')
        fs.writeFileSync(
            this.containersDir + '/index.js',
            content
        )

    }

    create() {

        let fileContent = this._buildFileContent()
        this._writeFile(fileContent)
        // build the associated component
        this._buildComponent()
        this._buildActionsReducers()
        this._addImportToIndex()

    }

}


module.exports = BarnumContainer
