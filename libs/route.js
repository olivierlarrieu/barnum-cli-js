const fs = require('fs')
const TemplateFromFile = require('./utils/mustache')
const BarnumProject = require('./project')
const BarnumContainer = require('./container')
const BarnumComponent = require('./component')

class Data {

    constructor() {

        this.defaultDescription = {
            "isResource": false,
            "isRoute": true,
            "components": [
                {
                    "name": "Container"
                },
            ],
            "resources": [

            ],
            "routes": [],
            "containers": [
                {
                    "name": "Route",
                    "props": [
                        {
                            "name": "name",
                            "type": "string",
                            "default": "Route"
                        },
                    ]
                },
            ]
        }

    }

}


class BarnumRoute extends Data {

    constructor(name, project_name) {

        super()

        const CWD = process.cwd()
        const SRC = CWD + '/src/'
        const ROUTE_DIRNAME = "Routes"

        this.name = name
        this.project_name = project_name
        this.projectDir = SRC + project_name
        this.routesDir = SRC + ROUTE_DIRNAME

        const description = this._prepareDescription()
        this.project = new BarnumProject(description, `${this.routesDir}/${this.name}`)

    }

    _prepareDescription() {

        let description =  this.defaultDescription
        description.name = this.name
        let containers = []
        let components = []
        description.components.forEach((obj) => {

            let newcmp = new BarnumComponent(obj.name, `Routes/${this.name}`)
            components.push(newcmp)

        })
        description.components = components

        description.containers.forEach((obj) => {

            obj.props[0].default = this.name
            containers.push(new BarnumContainer(this.name, obj.props, `Routes/${this.name}`))

        })
        description.containers = containers
        return description

    }

    addRouteToProject() {

        let resourceTemplate = new TemplateFromFile(
            `${this.projectDir}/resources.js`
        )
        let content = resourceTemplate.render().split(/\r\n|\n/)
        let final_content = []

        content.forEach((line) => {

            final_content.push(line)
            if (line.includes('const Resources = [')) {

                let new_line_1 = `    name: '${this.name}',`
                let new_line_2 = `    icon: Icon,`
                let new_line_3 = `    path: '/${this.name.toLowerCase()}',`
                let new_line_4 = `    component: LoadableComponent(() => { return import('${this.name}/app.route') })`
                final_content.push('  {')
                final_content.push(new_line_1)
                final_content.push(new_line_2)
                final_content.push(new_line_3)
                final_content.push(new_line_4)
                final_content.push('  },')

            }

        })

        fs.writeFileSync(
            resourceTemplate.path,
            final_content.join('\n')
        )

    }

    _addImportToApp() {

        let resourceTemplate = new TemplateFromFile(
            `${this.routesDir}/${this.name}/app.route.js`
        )
        let content = resourceTemplate.render({name: this.name})
        resourceTemplate.write(content)

    }

    create() {

        this.project.create()
        this.addRouteToProject()
        this._addImportToApp()

    }

}

module.exports = BarnumRoute
