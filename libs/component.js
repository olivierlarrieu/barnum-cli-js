const fs = require('fs')
const TemplateFromFile = require('./utils/mustache')


class BarnumComponent {

    constructor(name, projectName) {

        const CWD = process.cwd()
        const SRC_DIRNAME = process.env.BARNUM_SOURCE_DIRNAME
        const SRC = CWD + `/${SRC_DIRNAME}/`

        this.name = name
        this.projectName = projectName
        this.componentsDir = SRC + this.projectName + '/components'
        this.projectDir = SRC + this.projectName
        this.template = this._getTemplate()

    }

    _getGlobalCssTemplate() {

        return new TemplateFromFile(
            this.projectDir + '/main.css'
        ).render().split(/\r\n|\n/)

    }

    _getTemplate() {

        return new TemplateFromFile(
            __dirname + '/templates/react_component.js'
        ).render(
            { name: this.name }
        )

    }

    _writeFile(content) {
        fs.mkdirSync(this.componentsDir + '/' + this.name)
        fs.writeFileSync(
            this.componentsDir + '/' + this.name + '/index.js',
            content
        )

    }

    _buildFileContent() {

        return this.template

    }

    _addImportToIndex() {

        let components = fs.readdirSync(this.componentsDir)
        let final_content = []
        let exports_name = []

        components.forEach((cmp) => {

            if (cmp.includes('.js') && cmp != 'index.js') {

                let name = cmp.split('.js')[0]
                let new_line = `import { ${name} } from './${name}'`
                final_content.push(new_line)
                exports_name.push(name)

            }

        })

        let exports_line = `\n\nexport {${exports_name.join(', ')}}\n`
        final_content.push(exports_line)
        let content = final_content.join('\n')
        fs.writeFileSync(
            this.componentsDir + '/index.js',
            content
        )

    }

    _contributeToMainCss() {

        let css_content = this._getGlobalCssTemplate()
        let new_css_line = `\n.${this.name}Component {\n\n}`
        css_content.push(new_css_line)

        fs.writeFileSync(this.projectDir + '/main.css', css_content.join('\n'))

    }

    create() {

        let content = this._buildFileContent()
        this._writeFile(content)
        this._addImportToIndex()
        this._contributeToMainCss()

    }

}

module.exports = BarnumComponent
